# Giphy Project

The Giphy Project is a web application that allows users to search, view, and interact with GIFs from the Giphy API.

## Getting Started

These instructions will help you get a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Before running the project, make sure you have the following software installed:

- Node.js
- NPM (Node Package Manager)

### Installation
  1. Clone the repository:
    git clone https://gitlab.com/danaellaz/giphy-project.git

  2. Navigate to the project directory:
    cd giphy-project

  3. Install the dependencies:   
    npm install

### Configuration

1. Obtain a Giphy API key by creating an account on the Giphy Developers website.

2. In the project directory, create a `.env` file and add the following line:

    API_KEY=YOUR_GIPHY_API_KEY

The application will be accessible at `http://localhost:3000` in your web browser.

### Features

- Home Page: Displays trending GIFs and provides navigation to other sections.
- Trending Gifs: Shows the trending GIFs based on the Giphy API.
- Favorite Gifs: Allows users to mark their favorite GIFs and view them later.
- Uploaded Gifs: Displays GIFs uploaded by the user.
- Upload Gif: Enables users to upload their own GIFs to the platform.
- Search: Allows users to search for GIFs based on a specific keyword.

### Contributing

Contributions to the project are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request.

### License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

### Acknowledgements

- [Giphy API](https://developers.giphy.com/)
- [Node.js](https://nodejs.org/)
- [Express.js](https://expressjs.com/)
