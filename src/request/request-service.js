/* eslint-disable func-style */
import { uploadPopMenu } from '../events/navigation-events.js';
import { trendingFn } from '../events/trending-events.js';

/**
 * The Giphy API key.
 *

 */

// const API_KEY = 'iq6IDVYTJQy2JNfmxUxmnEXMgdCadCiS';
const API_KEY = '9DsRigK3Aao3ecdu09Fu85lgL4gpe88s'; // Kris
// const API_KEY = '5NuY4K6scua6yKjIvTtQkKV0cxQlvimv'; // Dana

/**
 * The offset for trending GIFs.
 */
const offset = 0;


/**
 * The limit for searched GIFs.
 */
export let limit = 20;

/**
 *  Toggle trending gifs/stickers.
 */
export let param = 'gifs';


/**
 * Get searched GIFs based on the provided search term.
 * @param {string} searchTerm - The search term for GIFs.
 * @return {Promise<Array>} - A promise that resolves to an array of GIF data.
 */
export const getSearchedGifs = (searchTerm = '') => {
  let url = `https://api.giphy.com/v1/gifs/search?api_key=${API_KEY}&limit=${limit}&q=`;
  const str = searchTerm.trim();
  url = url.concat(str);
  const result = fetch(url).then(response => response.json())
    .then(content => {
      return content.data;
    });

  return result;
};

/**
 * Get GIF data by ID.
 * @param {string} gifId - The ID of the GIF.
 * @return {Promise<Object>} - A promise that resolves to the GIF data.
 */
export const getGifById = (gifId = '') => {
  const url = `https://api.giphy.com/v1/gifs/${gifId}?api_key=${API_KEY}&rating=g`;
  const result = fetch(url).then(response => response.json());
  return result;
};


/**
 * Get multiple GIF data by IDs.
 * @param {string} gifsId - Comma-separated IDs of the GIFs.
 * @return {Promise<Array>} - A promise that resolves to an array of GIF data.
 */
export const getGifsById =async (gifsId = '') => {
  const transformedString = gifsId.split(',').map(item => encodeURIComponent(item)).join('%2C');
  const url = `https://api.giphy.com/v1/gifs?api_key=${API_KEY}&ids=${transformedString}&rating=g`;
  const result =await fetch(url).then(response => response.json());
  return await result;
};

export const getRandomGif = () => {
  const endpoint = `https://api.giphy.com/v1/randomid?api_key=${API_KEY}`;

  return fetch(endpoint)
    .then(response => response.json())
    .then(data => {
      const randomId = data.data.random_id;
      // Use the random ID to make a request for a random GIF
      return fetchRandomGif(randomId);
    })
    .catch(error => {
      console.error('Error:', error);
    });

  function fetchRandomGif(randomId) {
    const randomGifEndpoint = `https://api.giphy.com/v1/gifs/random?api_key=${API_KEY}&random_id=${randomId}`;

    return fetch(randomGifEndpoint)
      .then(response => response.json())
      .then(data => {
        const randomGif = data.data;
        return randomGif;
      })
      .catch(error => {
        console.error('Error:', error);
      });
  }
};

/**
 * Upload a GIF file.
 * @param {Event} event - The form submit event.
 * @return {Promise<Object>} - A promise that resolves to the uploaded GIF data.
 */
export const uploadGif = async (event) => {
  event.preventDefault();

  const userFile = document.getElementById('file');
  const file = userFile.files[0];
  const titleInput = document.getElementById('title');
  const title = titleInput.value;

  const formData = new FormData();
  formData.append('file', file);
  formData.append('api_key', API_KEY);
  formData.append('tags', title);


  await fetch(`https://upload.giphy.com/v1/gifs`,
    {
      method: 'POST',
      body: formData,
    })
    .then(res => res.json())
    .then(data => {
      const dataId = data.data.id;
      localStorage.setItem('gifId', dataId);

      const fileIds = JSON.parse(localStorage.getItem('fileIds')) || [];
      fileIds.push(dataId);
      localStorage.setItem('fileIds', JSON.stringify(fileIds));
      console.log(fileIds);
      console.log(localStorage);
      return { success: true, gifId: dataId };
    })
    .then(result => {
      if (result.success === true) {
        // eslint-disable-next-line no-unused-vars
        const popUpMenu = uploadPopMenu();
      }
    })
    .catch(e => console.log(e));
};


/**
 * Get trending GIFs.
 * @return {Promise<Object>} - A promise that resolves to the trending GIF data.
 */
export async function trendingGifs() {
  const response = await fetch(`https://api.giphy.com/v1/${param}/trending?&api_key=${API_KEY}&limit=${limit}&offset=${offset}`);
  return await response.json();
};

/**
 * Increase limit with 10
 */
export const changeLimit= () => {
  limit += 10;
  trendingFn();
};

/**
 * Function wich toggles between stickers and gifs
 */
export const showStickers = () => {
  param = param === 'stickers' ? 'gifs' : 'stickers';
  trendingFn();
};
