export const HOME = 'home';

export const TRENDING = 'trending';

export const FAVORITES = 'favorites';

export const UPLOADED = 'uploaded';

export const UPLOAD = 'upload';

export const ABOUT = 'about';

export const CONTAINER_SELECTOR = '.container';

export const FULL_HEART = '❤';

export const EMPTY_HEART = '♡';


