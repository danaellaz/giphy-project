export const aboutPageView = () => {
  return `
    <div class='about'>
        <div class='about-container>
            <h1 class='title-info'>
                <span class='main-title'>About our Project</span>
            </h1>
            <div class='about-info'>
                <div class='about-txt'>
                    <span class=''search-gif-txt'>
                        Discover a vast collection of animated expressions, easily search and find your perfect GIF,
                        keep up with the latest trends, save your favorites, and even upload and share your own creations.
                        Join our vibrant community and let the GIFs speak louder than words! 
                    </span>
                </div>
                <span class='our-team'>OUR TEAM</span>
                <section id='about-team'>
                    <div class='user'>
                        <img src='./src/files/minion-one.gif' class='img' alt='Profile picture of ###'>
                        <div class='data'>
                            <div class='text-user'>Nikola Ninov</div>
                        </div>
                    </div>
                    <div class='user'>
                        <img src='./src/files/minion-two.gif' class='img' alt='Profile picture of ###'>
                        <div class='data'>
                            <div class='text-user'>Danaela Zlateva</div>
                        </div>
                    </div>
                    <div class='user'>
                        <img src='./src/files/minion-three.gif' class='img' alt='Profile picture of ###'>
                        <div class='data'>
                            <div class='text-user'>Kristiyan Todorov</div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    `;
};
