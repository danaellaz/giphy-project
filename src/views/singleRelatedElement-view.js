/**
 * Generates HTML for a single related GIF element.
 *
 * @param {Object} element - The related GIF element data.
 * @return {string} The generated HTML string.
 */
export const singleRelatedElement = (element) => {

  return `    
    <div class="card-container gif-link" data-id="${element.id}">
        <div class="card-relative">
            <div class="relative-img">
                <img  src="${element.images.fixed_height.url}" alt="${element.title}" />
            </div>
            <div class="relative-info">
            ${element.title.substring(0, element.title.indexOf('GIF')).trim()||
            element.slug.split('-')[0]}
            </div>
        </div>
    </div>
    `;
};
