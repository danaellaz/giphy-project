import { singleElement } from './singleElement-view.js';


/**
 * Converts an array of data representing searched elements into an HTML string.
 * Each element in the array is transformed using the `singleElement` function.
 *
 * @param {Array} data - The array of data representing searched elements.
 * @return {string} The generated HTML string.
 */
export const toAllSearchedElements = (data) => `
    <div class='all-from-search'>
      <div class="out">
      ${data.length === 0 ? '<h1>No search results</h1>' :
    data.map(el => singleElement(el)).join('')
}
    </div>
  </div>
  `;
