import { limit, param } from '../request/request-service.js';
import { singleElement } from './singleElement-view.js';

/**
 *
 * @param {data} res
 * @returns the gif with the information for it
 */

export const trendingView = (res) => `
    <div class='all-from-search'>
      <div class='seeMoreWrapper'>
      <button class='trendingBtn' id='switch'>${param}</button>
      </div>
      <div class="out">
        ${res.data.map(element => singleElement(element)).join('')};
        </div>
        ${limit <= 50 ? ` 
        <div class='seeMoreWrapper'>
          <button class='trendingBtn' id='seeMore'>See more</button>
        </div>` : ''}
    </div>
    `;
