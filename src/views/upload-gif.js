/**
 * Generates HTML for the upload GIF view.
 *
 * @return {string} The generated HTML string.
 */
export const uploadGifView = () => {
  return `
    <div class='container-uploaded'>
        <form id="form">
            <label for="title" class="file-label">                
                <span class="custom-button">Title:</span>
                <input type="text" id="title">
            </label>
            <label for='file' class='file-label'>                
                <input type="file" id="file" accept=".gif">
                <span class="custom-button">Choose file</span>
            </label>
            <button type="submit" id='submit-btn'>Upload file
                <span class='btn-icon'>
                    <ion-icon name="cloud-upload-outline"></ion-icon>
                </span>
            </button>
        </form>
    </div>
    `;
};
