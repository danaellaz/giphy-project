// import { EMPTY_HEART } from "../common/constants.js";
import { getFavoriteStatus } from '../events/favorites-events.js';

/**
 * Generates HTML for a random GIF element.
 *
 * @param {Object} element - The GIF element data.
 * @return {string} The generated HTML string.
 */
export const randomElement = (element) => {
  console.log(element);
  return `
  <div class='favorite-gifs'>  
    <div class='all-from-search'>      
        <h1 id='no-fav-element'>There are no favorite gifs to display</h1>
            <div class="out"> 
                <div class="article">
                    <figure class="gif-link article-image">         
                        <img class="gif-link" data-id="${element.id}" src="${element.images.downsized.url}" alt="${element.title}">    
                    </figure>
                    <div class="article-card">
                        <h2 class="card-title">
            ${element.title.substring(0, element.title.indexOf('GIF')).trim()||
            element.slug.split('-')[0]}
                        </h2>
                        ${getFavoriteStatus(element.id)}
                    </div>
                </div>    
            </div> 
        </div> 
    </div> 
  `;
};
