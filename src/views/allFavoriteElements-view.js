import { singleElement } from './singleElement-view.js';

/**
 * Converts an array of data representing favorite elements into an HTML string.
 * Each element in the array is transformed using the `singleElement` function.
 *
 * @param {Array} data - The array of data representing favorite elements.
 * @return {string} The generated HTML string.
 */
export const toAllFavoriteElements = (data) => {
  return `
    <div class='favorite-gifs'>  
      <div class='all-from-search'>      
        <div class="out">                   
          ${data.map(el => singleElement(el)).join('')}    
      </div>
    </div>
  </div>
  `;
};
