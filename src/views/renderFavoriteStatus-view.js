/**
 * Renders the favorite status for a GIF.
 *
 * @param {Array<string>} data - An array containing the IDs of the GIFs that are marked as favorites.
 * @param {string} gifId - The ID of the GIF for which the favorite status is being rendered.
 * @returns {string} The HTML string representing the favorite status of the GIF.
 * @throws {TypeError} If the `data` parameter is not an array or the `gifId` parameter is not a string.
 *
 * @example
 * const data = ['gif1', 'gif2', 'gif3'];
 * const gifId = 'gif2';
 * const favoriteStatus = renderFavoriteStatus(data, gifId);
 * // Returns: '<span class="heart active" data-id="gif2">FULL_HEART</span>'
 *
 * @example
 * const data = [];
 * const gifId = 'gif4';
 * const favoriteStatus = renderFavoriteStatus(data, gifId);
 * // Returns: '<span class="heart" data-id="gif4">EMPTY_HEART</span>'
 */
import { EMPTY_HEART, FULL_HEART } from '../common/constants.js';

export const renderFavoriteStatus = (data, gifId) => {
  return data.includes(gifId) ?
    `<span class="heart active" data-id="${gifId}">${FULL_HEART}</span>` :
    `<span class="heart" data-id="${gifId}">${EMPTY_HEART}</span>`;
};
