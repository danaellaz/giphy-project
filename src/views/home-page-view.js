/**
 * Generates HTML for the home page view.
 *
 * @return {string} The generated HTML string.
 */
export const homePageView = () => {
  const searchEl = document.querySelector('.search-form');
  searchEl.style.marginTop = '40rem';
  const searchElWidth = document.querySelector('.search-input');
  searchElWidth.style.width = '40%';
  searchEl.style.right = '0rem';
  return `
  <div class='home-text'>
    <h1>Welcome To GIPHY By Team 14!<h1>
    <h2>The best application for searching and sharing funny GIFs with your friends!</h2>
    </div>
  </div>  
    `;
};
