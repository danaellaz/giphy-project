import { EMPTY_HEART } from '../common/constants.js';
import { getFavoriteStatus } from '../events/favorites-events.js';
import { singleRelatedElement } from './singleRelatedElement-view.js';

/**
 * Generates HTML for a gif element with additional details and related gifs.
 *
 * @param {Object} element - The main gif element data.
 * @param {Array} relatedElements - An array of related gif elements.
 * @return {string} The generated HTML string.
 */
export const detailsForGifElement = (element, relatedElements) => {
  const username = element.user ? element.user.username : 'unknown user';
  // eslint-disable-next-line max-len
  const avatarUrl = element.user ? element.user.avatar_url : 'https://st3.depositphotos.com/2546551/18320/v/600/depositphotos_183201814-stock-illustration-anonymous-male-profile-picture.jpg';


  return `
     <div class='card'>    
        <div class="user">
         <h2>@${username} </h2>
         <img src="${avatarUrl}" alt="user avatar" />
        </div>
        <div class="gif-display">
          <div class="image">
            <img src="${element.images.downsized.url}" alt="${element.title}">
          </div>
        <div class="gif-info">
          <div class="info-item">
            Title <span>${element.title.substring(0, element.title.indexOf('by')).trim()||
             element.slug.split('-')[0]}</span> 
            Type <span>${element.type}</span>
            ${getFavoriteStatus(element.id)}
          </div>             
        </div>
        <div class="related">
          <h3>Related GIFs</h3>
            <div class="carousel-container">
              <div class="carousel-inner">
                <div class="track">
                  ${relatedElements.map(el => singleRelatedElement(el)).join('')}    
                </div>
              </div>
              <div class="nav-carol">
                <button class="prev">
                  <i class="material-icons">
                    keyboard_arrow_left
                  </i>
                </button>
                <button class="next">
                  <i class="material-icons">
                    keyboard_arrow_right
                  </i>
                </button>
              </div>
            </div>
          </div>      
        </div>    
      </div>
  `;
};
