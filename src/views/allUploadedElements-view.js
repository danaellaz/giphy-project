import { singleElement } from './singleElement-view.js';

/**
 * Converts an array of data representing uploaded elements into an HTML string.
 * Each element in the array is transformed using the `singleElement` function.
 *
 * @param {Array} data - The array of data representing uploaded elements.
 * @return {string} The generated HTML string.
 */
export const toAllUploadedElements = (data) => `
     
      <div class='all-from-search'>   
        <div class="out">               
          ${data.map(el => singleElement(el)).join('')}    
      </div>
    </div>  
  `;
