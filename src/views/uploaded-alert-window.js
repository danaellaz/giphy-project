/**
 * Creates a modal window to display an alert message with an 'OK' button.
 *
 * @return {HTMLElement} The modal window as an HTML element containing the alert message and the 'OK' button.
 *
 * @example
 * const modalElement = alertWindow();
 * document.body.appendChild(modalElement);
 * // This will create a modal window with the alert message and 'OK' button appended to the document body.
 */
export const alertWindow = () => {
  const modal = document.createElement('div');
  modal.id = 'myModal';
  modal.innerHTML =
    `
        <p id='alert-txt'>Your file has been uploaded! Click 'OK' to go to 'Uploaded Gifs'.</p>
        <button id="ok-btn">OK</button>
    `;
  return modal;
};
