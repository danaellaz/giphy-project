/* eslint-disable func-style */

import { trendingView } from '../views/trending-view.js';
import { trendingGifs } from '../request/request-service.js';
import { q } from '../events/helpers.js';

/**
 * parsing the gifs in the container
 */
export async function trendingFn() {
  const result = await trendingGifs();
  const content = q('.container');
  content.innerHTML= trendingView(result);
};
