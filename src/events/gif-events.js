import { getGifById, getSearchedGifs } from '../request/request-service.js';
import { detailsForGifElement } from '../views/elementDetails-view.js';

let index = 0;

/**
 * Get the details for a specific GIF and display them.
 * @param {string} gifId - The ID of the GIF to fetch details for.
 */
export const gifDetails = async (gifId) => {
  // Fetch data
  const gifDataFromApi = await getGifById(gifId);
  const dashIndex=gifDataFromApi.data.slug.indexOf('-');
  let title = gifDataFromApi.data.title.substring(0, gifDataFromApi.data.title.indexOf('by')).trim() || gifDataFromApi.data.slug.substring(0, dashIndex);
  if (!gifDataFromApi.data.title) {
    title = gifDataFromApi.data.slug;
  }
  const reachForRelated = await getSearchedGifs(title);
  // Build view
  const detailsElement = detailsForGifElement(gifDataFromApi.data, reachForRelated);
  const main = document.querySelector('main');
  // Display
  main.innerHTML = detailsElement;
};

/**
 * Render the carousel by moving to the previous or next item.
 * @param {string} buttonName - The name of the button clicked ('prev' or 'next').
 */
export const renderCarousel = (buttonName) => {
  const prev = document.querySelector('.prev');
  const next = document.querySelector('.next');
  const track = document.querySelector('.track');
  let carouselWidth = document.querySelector('.carousel-container').offsetWidth;
  window.addEventListener('resize', () => {
    carouselWidth = document.querySelector('.carousel-container').offsetWidth;
  });

  if (buttonName === 'next') {
    index++;
    prev.classList.add('show');
    track.style.transform = `translateX(-${index * carouselWidth}px)`;
    if (track.offsetWidth - (index * carouselWidth) < (carouselWidth + 400)) {
      next.classList.add('hide');
    }
  } else {
    index--;
    next.classList.remove('hide');
    if (index === 0) {
      prev.classList.remove('show');
    }
    track.style.transform = `translateX(-${index * carouselWidth}px)`;
  }
};
