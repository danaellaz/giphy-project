import { CONTAINER_SELECTOR, FAVORITES, HOME, TRENDING, UPLOAD, UPLOADED, ABOUT } from '../common/constants.js';
import { homePageView } from '../views/home-page-view.js';
import { trendingFn } from '../events/trending-events.js';
import { uploadGifView } from '../views/upload-gif.js';
import { gifFavorites } from './favorites-events.js';
import { q, searchPosition, setActiveNav } from './helpers.js';
import { uploadGif } from '../request/request-service.js';
import { uploadedGifs } from './uploaded-gif-event.js';
import { alertWindow } from '../views/uploaded-alert-window.js';
import { aboutPageView } from '../views/about.js';


/**
 * Load and render the specified page.
 * @param {string} page - The name of the page to load and render.
 * @return {function } || null
 */
export const loadPage = (page = '') => {

  const video = document.getElementById('video');
  video.src = '';

  switch (page) {
  case HOME:
    video.src = './src/files/pexels-hand-phone-2.mp4';
    setActiveNav(HOME);
    return renderHome();
  case TRENDING:
    setActiveNav(TRENDING);
    return renderTrending();
  case FAVORITES:
    setActiveNav(FAVORITES);
    return renderFavorites();
  case UPLOADED:
    setActiveNav(UPLOADED);
    return renderUploaded();
  case UPLOAD:
    setActiveNav(UPLOAD);
    return renderUpload();
  case ABOUT:
    setActiveNav(ABOUT);
    return aboutPage();

  default:
    return null;
  }
};

/**
 * Render the home page.
 */
export const renderHome = () => {
  const htmlContent = homePageView();

  q(CONTAINER_SELECTOR).innerHTML = htmlContent;
};

/**
 * Render the trending page.
 */
export const renderTrending = () => {
  searchPosition();

  const trendingContent = trendingFn();
  q(CONTAINER_SELECTOR).innerHTML = trendingContent;
};

/**
 * Render the favorites page.
 */
export const renderFavorites = () => {
  searchPosition();
  gifFavorites();
};

/**
 * Render the uploaded page.
 */
export const renderUploaded = () => {
  searchPosition();

  // eslint-disable-next-line no-unused-vars
  const htmlContent = uploadedGifs();
};

/**
 * Render the upload page.
 */
export const renderUpload = () => {
  searchPosition();

  const uploadGifContent = uploadGifView();

  q(CONTAINER_SELECTOR).innerHTML = uploadGifContent;

  document.getElementById('form').addEventListener('submit', uploadGif);
  console.log('Upload page');
};

/**
 * Render an alert message after the GIF has been uploaded
 * and redirects user to the 'Uploaded Gifs'
 */
export const uploadPopMenu = () => {
  const alert = alertWindow();

  document.querySelector('.container').appendChild(alert);

  // eslint-disable-next-line no-unused-vars
  const okBtn = alert.querySelector('#ok-btn').addEventListener('click', () => {
    loadPage(UPLOADED);
  });
};

export const aboutPage = () => {
  searchPosition();
  const about = aboutPageView(); // some function which defines the rendered view

  q(CONTAINER_SELECTOR).innerHTML = about;
  console.log(`about page`);
};

