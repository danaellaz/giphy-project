import { getGifsById } from '../request/request-service.js';
import { q } from './helpers.js';
import { CONTAINER_SELECTOR } from '../common/constants.js';
import { toAllUploadedElements } from '../views/allUploadedElements-view.js';

/**
 * Fetch the uploaded GIFs from the server and render them on the page.
 */

export const uploadedGifs = async () => {
  const fileIds = JSON.parse(localStorage.getItem('fileIds'));
  const items = Array.isArray(fileIds) ? fileIds.join(',') : '';
  if (items === '') {
    const html =`<h1 class='no-uploads'>There are None Uploaded Gifs for the moment.</h1>`;
    q(CONTAINER_SELECTOR).innerHTML = html;
  } else {
    const gifsFromServer = await getGifsById(items);
    const html = toAllUploadedElements(gifsFromServer.data);
    q(CONTAINER_SELECTOR).innerHTML = html;
  }
};
