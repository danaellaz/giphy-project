/**
 * Shorthand for document.querySelector
 * @param {string} selector
 * @return {Element}
 */
export const q = (selector) => document.querySelector(selector);

/**
 * Shorthand for document.querySelectorAll
 * @param {string} selector
 * @return {NodeLists<Element>}
 */
export const qs = (selector) => document.querySelectorAll(selector);

export const setActiveNav = (page) => {
  const navLinks = qs('.navbar .nav-link');

  Array.from(navLinks).forEach(link => {
    const dataPage = link.getAttribute('data-page');
    if (dataPage === page) {
      link.classList.add('active');
    } else {
      link.classList.remove('active');
    }
  });
};

export const searchPosition = () => {
  const searchEl = document.querySelector('.search-form');
  searchEl.style.marginTop = '5rem';
  searchEl.style.position = 'relative';
  searchEl.style.right = '-12rem';
  const searchElWidth = document.querySelector('.search-input');
  searchElWidth.style.width = '80%';
};
