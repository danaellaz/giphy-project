import { getSearchedGifs } from '../request/request-service.js';
import { toAllSearchedElements } from '../views/allSearchedElements-view.js';
import { searchPosition } from './helpers.js';

/**
 * Search for GIFs based on the specified search term and render the search results.
 * @param {string} searchTerm - The term to search for GIFs.
 */
export const searchGif = async (searchTerm) => {
  const dataFromSearchEvent = await getSearchedGifs(searchTerm);

  const all = toAllSearchedElements(dataFromSearchEvent);

  const main = document.querySelector('main');
  main.innerHTML = all;
  searchPosition();
};
