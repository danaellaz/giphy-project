/* eslint-disable valid-jsdoc */
import { CONTAINER_SELECTOR, EMPTY_HEART, FAVORITES, FULL_HEART } from '../common/constants.js';
import { getGifsById, getRandomGif } from '../request/request-service.js';
import { toAllFavoriteElements } from '../views/allFavoriteElements-view.js';
import { randomElement } from '../views/randomElement-view.js';
import { renderFavoriteStatus } from '../views/renderFavoriteStatus-view.js';
import { q } from './helpers.js';
import { loadPage } from './navigation-events.js';

let favorites = JSON.parse(localStorage.getItem('favorites')) || [];

/**
 * Add a GIF to the list of favorites.
 * @param {string} gifId - The ID of the GIF to add.
 */
const addFavorite = (gifIf) => {
  if (favorites.find(id => id === gifIf)) {
    return;
  }
  favorites.push(gifIf);
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

/**
 * Remove a GIF from the list of favorites.
 * @param {string} gifId - The ID of the GIF to remove.
 */
const removeFavorite = (gifIf) => {
  favorites = favorites.filter(id => id !== gifIf);
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

/**
 * Get the list of favorite GIFs.
 * @return {string[]} - An array of favorite GIF IDs.
 */
const getFavorites = () => [...favorites];

/**
 * Toggle the favorite status of a GIF.
 * @param {string} gifId - The ID of the GIF to toggle.
 */
export const toggleFavoriteStatus = (gifId) => {
  // eslint-disable-next-line no-shadow
  const favorites = getFavorites();
  const heartSpan = q(`span.heart[data-id="${gifId}"]`);
  const divFavsElement = q('.favorite-gifs');

  if (favorites.includes(gifId)) {
    removeFavorite(gifId);
    heartSpan.classList.remove('active');
    heartSpan.innerHTML = EMPTY_HEART;
    if (divFavsElement !== null) {
      loadPage(FAVORITES);
    }
  } else {
    addFavorite(gifId);
    heartSpan.classList.add('active');
    heartSpan.innerHTML = FULL_HEART;
    if (divFavsElement !== null) {
      loadPage(FAVORITES);
    }
  }
};

/**
 * Render the favorite status for a GIF.
 * @param {string} gifId - The ID of the GIF to render the status for.
 * @return {string} - The HTML string representing the favorite status.
 */
export const gifFavorites = async () => {
  // get items from localStorage
  const favoriteItemsFromLocalStorage = JSON.parse(localStorage.getItem('favorites'));
  const items = Array.isArray(favoriteItemsFromLocalStorage) ? favoriteItemsFromLocalStorage.join(',') : '';

  if (items === '') {
    const random = await getRandomGif();
    // pass data from localStorage to request service
    const html = randomElement(random);
    q(CONTAINER_SELECTOR).innerHTML = html;
  } else {
    const gifsFromServer = await getGifsById(items);
    // pass data from localStorage to request service
    const html = toAllFavoriteElements(gifsFromServer.data);
    q(CONTAINER_SELECTOR).innerHTML = html;
  }
};

/**
 * Display the favorite GIFs.
 */
export const getFavoriteStatus = (gifId) => {
  // eslint-disable-next-line no-shadow
  const favoriteItems = getFavorites();
  const renderedStatus = renderFavoriteStatus(favoriteItems, gifId);
  return renderedStatus;
};
