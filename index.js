/**
 * @file Entry point JavaScript file for the Giphy web application.
 * @module index.js
 */
import { HOME } from './src/common/constants.js';
import { toggleFavoriteStatus } from './src/events/favorites-events.js';
import { gifDetails, renderCarousel } from './src/events/gif-events.js';
import { loadPage } from './src/events/navigation-events.js';
import { searchGif } from './src/events/search-events.js';
import { changeLimit, showStickers } from './src/request/request-service.js';


/**
 * Initializes the Giphy web application when the DOM content is loaded.
 */
document.addEventListener('DOMContentLoaded', () => {
  /**
    * Event listener for the search button.
    *
    * @param {Event} event - The click event.
    */
  document.getElementById('btnSearch').addEventListener('click', (event) => {
    event.preventDefault();
    if (event.target.id === 'btnSearch') {
      const inputSearch = document.getElementById('search');
      const searchValue = inputSearch.value;
      searchGif(searchValue);
      inputSearch.value = '';
    }
  });

  /**
     * Event listener for click events on the document.
     *
     * @param {Event} event - The click event.
     */
  document.addEventListener('click', (event) => {
    if (event.target.classList.contains('nav-link')) {
      loadPage(event.target.getAttribute('data-page'));
    }

    if (event.target.classList.contains('next')) {
      renderCarousel('next');
    }
    if (event.target.classList.contains('prev')) {
      renderCarousel('prev');
    }


    if (event.target.classList.contains('gif-link')) {
      const gifId = event.target.dataset.id;
      gifDetails(gifId);
    }

    if (event.target.classList.contains('heart')) {
      const gifId = event.target.dataset.id;
      toggleFavoriteStatus(gifId);
    }

    if (event.target.id === 'seeMore') {
      changeLimit();
    }

    if (event.target.id === 'switch') {
      showStickers();
    }

  });
  loadPage(HOME);

});
